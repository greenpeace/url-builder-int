<?php

function clean_input( $data, $size ) {
    $data = trim($data);
    $data = substr($data, 0, $size);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function generate_json_output( $output ) {
    
    if ( isset ( $_SERVER['HTTP_ORIGIN'] ) ) {
        $origin = $_SERVER['HTTP_ORIGIN'];
    } else {
        $origin = "Unknown";
    }

    $output = json_encode( $output);
    header('Content-Type: application/json');
    header("Access-Control-Allow-Origin: " . $origin );
    echo ($output);
}

?>