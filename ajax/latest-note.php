<?php

require_once('../libs/medoo.php');

require_once('../config/config.php');

require_once('../libs/functions.php');

function get_latest_note( $database, $limit = 5 ) {
    $datas = $database->select("notes",
        array ( "stime", "note"),
        array(
            "LIMIT" => $limit,
            "ORDER" => "stime DESC"
		)
    );
    
    return $datas;
}

$results = get_latest_note( $database, 1);

/* Prepare output */
$output = array();
$output =  $results;

/* Generate output */
generate_json_output( $output );

//var_dump( $output );

?>