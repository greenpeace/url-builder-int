<?php

require_once('../libs/medoo.php');

require_once('../config/config.php');

require_once('../libs/functions.php');

function get_latest_urls( $database, $limit = 5 ) {
    $datas = $database->select("savedurls",
        array ( "useremail", "stime", "origurl", "source", "medium", "campaign", "ucontent", "term", "bitly", "created"),
        array(
            "LIMIT" => $limit,
            "ORDER" => "stime DESC"
        )
    );
    
    return $datas;
}

$results = get_latest_urls( $database, 10);

/* Prepare output */
$output = array();
$output =  $results;

/* Generate output */
generate_json_output( $output );

//var_dump( $output );

?>