<?php

// Medoo
require_once('libs/medoo.php');

// Bitly API:
require_once("shortner/bitly.php");

// Goo.gl API
require_once("shortner/Googl.class.php");

// Configurations:
require_once('config/config.php');

// Server variables to store:

$path = $_SERVER['SCRIPT_NAME'];
$ip = addslashes( filter_var( $_SERVER['REMOTE_ADDR'] , FILTER_SANITIZE_STRING ) );
$stime = microtime(true);
$created = date("Ymd");

// Getting and validating the values from the post request and from the server:

$origurl = addslashes( filter_var( $_POST["origurl"], FILTER_SANITIZE_STRING ) );
$source = addslashes( filter_var( $_POST["source"], FILTER_SANITIZE_STRING ) );
$medium = addslashes( filter_var( $_POST["medium"], FILTER_SANITIZE_STRING ) );
$campaign = addslashes( filter_var( $_POST["campaign"], FILTER_SANITIZE_STRING ) );
$content = addslashes( filter_var( $_POST["content"], FILTER_SANITIZE_STRING ) );
$term = addslashes( filter_var( $_POST["term"], FILTER_SANITIZE_STRING ) );
$teaser = addslashes( filter_var( $_POST["teaser"], FILTER_SANITIZE_STRING ) );
$useremail = addslashes( filter_var( $_POST["useremail"], FILTER_SANITIZE_EMAIL ) );
$comment = addslashes( filter_var( $_POST["comment"], FILTER_SANITIZE_STRING ) );

// Extract # fragment from URL if it exists.
if( strpos ( $origurl , '#') ) {
	$fragment = substr( $origurl, strpos ( $origurl , '#') );
	$origurl = substr( $origurl, 0, strpos ( $origurl , '#') );
} else {
	
	$fragment = '';
	
}

// Add ? or & to given URL.
if ( strpos ( $origurl , '?') ) {

	$origurl .= '&';

} else {
	
	$origurl .= '?';

}

$finalurl = $origurl . "utm_medium=" . urlencode($medium) . "&utm_source=" . urlencode($source) . "&utm_campaign=" . urlencode($campaign) . "&utm_content=" . urlencode($content) . "&utm_term=" . urlencode($term) . $fragment;


if ( $useShortner === "bitly" ) {

	// Bitly generation:
	$bitly = new Bitly( $bitly_user , $bitly_key );
	$urlmin = $bitly->shorten( $finalurl );
	
} else if ( $useShortner === "googl" ) {
	
	// Goo.gl generation
	$googl = new Googl( $googl_key );
	$urlmin = $googl->shorten( $finalurl );
	
} else {
	
	// No short URL
	$urlmin = '';
	
}

// Write the MySQL database:

$status = "Started";

if ( !$database )
  {
	$status = "Failed to connect";   
  }
  else {
      
      $status = "Connected";

      $database->insert("savedurls", array(
	  "useremail" => $useremail,
	  "ip" => $ip,
	  "stime" => $stime,
	  "origurl" => $origurl,
	  "source" => $source,
	  "medium" => $medium,
	  "campaign" => $campaign,
	  "ucontent" => $content,
	  "term" => $term,
	  "ucomment" => $comment,
	  "bitly" => $urlmin,
	  "created" => $created
      ));  	
      
      $status = "Inserted";	
  }


  
// if medium is Social share links:

$showhideshare="show";
$showhideshort="hide";

if ($source=="facebook" and $medium=="social_network_link")
	{
		$socialshare = "http://www.facebook.com/sharer.php?u=" . urlencode($urlmin);
	}
elseif ($source=="tuenti" and $medium=="social_network_link") 
	{
		$socialshare = "http://www.tuenti.com/share?url=" . urlencode($urlmin);		
	}
elseif ($source=="meneame" and $medium=="social_network_link") 
	{
		$socialshare = "http://meneame.net/submit.php?url=" . urlencode($urlmin);		
	}
elseif ($source=="twitter" and $medium=="social_network_link" and $teaser!="") 
	{
		$socialshare = "https://twitter.com/intent/tweet?text=" . urlencode($teaser) . urlencode(" ") . urlencode($urlmin) . "&amp;source=webclient";		
	}
elseif ($source=="google-mas" and $medium=="social_network_link") 
	{
		$socialshare = "https://plus.google.com/share?url=" . urlencode($finalurl);		
	}
elseif ($source=="delicious" and $medium=="social_network_link" and $teaser!="") 
	{
		$socialshare = "http://delicious.com/post?url=" . urlencode($urlmin) . "&title=" . urlencode($teaser) ;		
	}
elseif ($source=="linkedin" and $medium=="social_network_link" and $teaser!="") 
	{
		$socialshare = "http://www.linkedin.com/shareArticle?mini=true&url=" . urlencode($urlmin) . "&title=" . urlencode($teaser) ;		
	}
else {
		$socialshare = "There's no automatic social share for this source";
		$teaser = "No teaser/tweet for this link ";
		$showhideshare="hide";
		$showhideshort="show";
}

// Formating the strings for the mail that the users can receive:

$comment = wordwrap($comment, 60);
$subject = $origurl;
$message = "URL: ". $origurl ."\n\n" . "Source: " . $source ."\n\n" . "Medium: " . $medium . "\n\n" . "Campaign: " . $campaign . "\n\n" . "Content: " . $content . "\n\n" . "Term: " . $term . "\n\n" . "Created by: " . $useremail . "\n\n" . "Comment:\n" . $comment . "\n\n" . "Final URL:\n". $finalurl . "\n\n" . "Short: " . $urlmin . "\n\n" . "Social link: " . $socialshare . "\n\n" . "Teaser/Tweet: " . $teaser  ;
$remetente = "From: " . "URL builder" . $sentfrom ;
$headers = $remetente . "\r\n" .  'Reply-To: ' . $sentfrom . "\r\n" . 'Content-Type: text/plain; charset=utf-8' . "\r\n" . 'X-Mailer: PHP/' . phpversion();

// Decisions on if the mail should be sent, sending:

if ( isset( $_POST["semail"] ) && $_POST["semail"]=="on" ) {
	$result = mail($useremail, $subject, $message, $headers);
} else {
	$result = false;
}

// Error detection and messages on mail:

if ($result==true) {
   $mymsg = 'Mail sent';
   $color = '#66cc00';
	}
else {
   $mymsg = 'Mail not sent';
   $color = '#ff4444';
	}

?>

<!DOCTYPE html>

<html lang="en">
<head>
	
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

      
	<title>URL Tools</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	
	<meta name="robots" content="noindex">
	
</head>

<body>
	
	<div class="container">
		
		<div class="row">
			
			<nav class="navbar navbar-default" role="navigation">
				<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="./">Tools URL</a>
					</div>
					
				  <!-- Collect the nav links, forms, and other content for toggling -->
				  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Tool<span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="backend.php">Backend</a></li>
								<li><a href="https://bitly.com/" target="_blank">Bit.ly</a></li>
								<li><a href="https://goo.gl/" target="_blank">Goo.gl</a></li>
								<li><a href="http://greenpeace.es/admin/" target="_blank">greenpeace.es/xxx</a></li>
								<li class="divider"></li>
								<li><a href="https://www.the-qrcode-generator.com/" target="_blank">QR codes</a></li>
								<li><a href="http://lab.osvaldo.pt/email-link-creator/" target="_blank">Mail links</a></li>
								<li><a href="https://support.google.com/analytics/answer/1033867?hl=en" target="_blank"> Google builder</a></li>
								<li><a href="http://urldecode.org/" target="_blank">URL encode/decode</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Google Analytics <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="https://www.google.com/analytics/web/" target="_blank">Home</a></li>
								<li class="divider"></li>
								<li><a href="https://www.google.com/analytics/web/#report/visitors-overview/a43128098w73107113p75474057/" target="_blank">Greenpeace Spain</a></li>
								<li><a href="https://www.google.com/analytics/web/#report/trafficsources-all-traffic/a43128098w73107113p75474057/" target="_blank">GP - All traffic</a></li>
								<li><a href="https://www.google.com/analytics/web/?hl=en#croverview/cr-overview/a43128098w73107113p75474057/" target="_blank">GP - Custom reports</a></li>
								<li class="divider"></li>
								<li><a target="_blank" href="https://www.google.com/analytics/web/template?uid=ZX6ihbdTSfijwDeVObD6zQ">Traffic by source/medium</a></li>
								<li><a target="_blank" href="https://www.google.com/analytics/web/template?uid=NvMYt4g2RdWYPhJtoq6Q3w">Traffic by campaign/page</a></li>

							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Other tools<span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a target="_blank" href="https://developers.facebook.com/tools/debug/">FB Sharing Debugger</a></li>
								<li><a target="_blank" href="https://cards-dev.twitter.com/validator">Twitter card validator</a></li>
								<li class="divider"></li>
								<li><a href="http://validator.w3.org/checklink" target="_blank">Link checker</a></li>
								<li><a href="https://search.google.com/structured-data/testing-tool" target="_blank">Check structured data</a></li>
								<li class="divider"></li>
								<li><a href="https://twitter.com/search-advanced?lang=en" target="_blank">Twitter search</a></li>
								<li><a href="https://www.google.com/webmasters/tools/" target="_blank">Webmaster tools</a></li>
							</ul>
						</li>
					</ul>
					
				  </div><!-- /.navbar-collapse -->
				</div><!-- /.container-fluid -->
			</nav>			
		</div><!-- /.row -->
		
	</div><!-- /.container -->

	<div class="container">
		
		<div class="row">
			
			<div class="col-md-8 col-lg-8">
				
				<noscript>
					You need to have javascript on.
				</noscript>
				
				<h2>URL</h2>
				
				<ul>
					<li><strong>Original URL:</strong> <a href="<?php echo $origurl ?>"><?php echo $origurl ?></a></li>
					<li><strong>Source:</strong> <?php echo $source ?></li>
					<li><strong>Medium:</strong> <?php echo $medium ?></li>
					<li><strong>Campaign:</strong> <?php echo $campaign ?></li>
					<li><strong>Ad/link content:</strong> <?php echo $content ?></li>
					<li><strong>Keyword:</strong> <?php echo $term ?></li>
					<li><strong>Your email:</strong> <?php echo $useremail ?></li>
				</ul>
				
				<h3>Comment:</h3>
				<p><?php echo $comment ?></p>
				
				<h3>Final link:</h3>
				<p id="finalurl"><?php echo $finalurl ?></p>
				<p><strong><a href="<?php echo $finalurl ?>" target="_blank"class="btn btn-primary">Test final link</a> &nbsp; <a href="javascript:history.go(-1);" class="btn btn-primary">Make another link</a></strong></p>
				
				<h3 class="<?php echo $showhideshort ?>">Shorts:</h3>
				<p class="shorty <?php echo $showhideshort ?>"><?php echo $urlmin ?></p>
				
				<h3 class="<?php echo $showhideshare ?>">Social Share:</h3>
				<p class="<?php echo $showhideshare ?> socialshare"><?php echo $socialshare ?></p>
				<p class="<?php echo $showhideshare ?> socialshare">Teaser/Tweet: <?php echo $teaser ?></p>
				
			</div>
			
			
			<div class="col-md-4 col-lg-4" style="background-color: #f4f4ff;">
			      <h3>Notes</h3>
			      
			      <form id="notes" name="notes" method="post" action="insert-note.php">
				      <fieldset>
					      <legend>Shared with the team:</legend>
					      
					      <div class="form.group">
						      <textarea id="note" name="note" maxlength="4000" rows="15" class="form-control" title="Notas compartidas con todos los usuarios."></textarea>
					      </div>
					      
				      </fieldset>
				      
				      <button type="submit" class="btn btn-primary">Save</button>
				      
			      </form>
			</div>
			
		</div><!-- /.row -->
		
	</div><!-- /.container -->

	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<footer>
					&copy; Osvaldo Gago
				</footer>
			</div>
		</div>
	</div>
	<script src="js/jquery-2.1.1.min.js"></script>
	<script src="js/jquery.validate.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/scripts.js"></script>
	
	<!-- <?php echo "DB status: " . $status . " | Mail: " . $mymsg  ?> -->
	
</body>
</html>
