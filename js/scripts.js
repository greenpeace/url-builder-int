/* jshint browser: true */
/* global $, jQuery, alert */

/* Validation Scripts */

$(document).ready(function(){

	$("#frm").validate({
		messages: {
			origurl: {
				required: "Don't forget the URL"
			},
			source: {
				required: "Don't forget the source"
			},
			medium: {
				required: "Don't forget the medium"
			},
			campaign: {
				required: "Don't forget the campaign"
			},
			content: {
				required: "Don't forget to fill this field"
			},
			term: {
				required: "Don't forget to fill this field"
			},
			useremail: {
				required: "Don't forget your email address",
				email: "A valid email address, please"
			}
		}
	});
});

/* ------------ */

$(document).ready(function(){
	$("#medium, #source").change(function(){
		var medium = $("#medium").val();
		var source = $("#source").val();
		
		if ( medium == "social_network_link" ) {
			$("#teaserfset").show("medium");
			
			if ( source=="twitter" ) {
				$("#teaserfset").show("medium");
				$("label[for=teaser]").text("Tweet:");					
			} else if ( source=="delicious" || source=="linkedin"  ) {
				$("#teaserfset").show("medium");
				$("label[for=teaser]").text("Title:");					
			} else {
				$("#teaserfset").hide("medium");					
			}
			
		} else {
			$("#teaserfset").hide("medium");			
		}
		
		if ( medium == "email" ) {
			$("label[for=content]").text("Keyword to identify the email:");
			$("#utm_content").attr("title", "Subject or mailing name. (utm_content)");	
			$("label[for=term]").text("Keyword to identify the link:");
			$("#utm_term").attr("title", "Link inside the message. (utm_term)");
				
		} else if ( medium == "social_network" || medium == "voluntasgp" ) {
			$("label[for=content]").text("Keyword to identify the post:");			
			$("#utm_content").attr("title", "Keyword to identify the post. (utm_content)");	
			$("label[for=term]").text("Tag:");
			$("#utm_term").attr("title", "Keyword to identify the audience (utm_term)");
			
		} else if ( medium == "social_network_link" ) {
			$("label[for=content]").text("Keyword to identify the page:");
			$("#utm_content").attr("title", "Keyword to identify the page. Ej. For example PetitionBees. (utm_content)");			
			$("label[for=term]").text("Keyword to identify the link:");
			$("#utm_term").attr("title", "Keyword to identify the link. (utm_term)");
			
		} else if ( medium == "app" ) {
			$("label[for=content]").text("Keyword to identify the app:");
			$("#utm_content").attr("title", "Keyword to identify the app. (utm_content)");	
			$("label[for=term]").text("Keyword to identify the link:");
			$("#utm_term").attr("title", "Keyword to identify the link. (utm_term)");
			
		} else if ( medium == "codigo-qr" || medium == "pdf" ) {
			$("label[for=content]").text("Keyword to identify the content.");
			$("#utm_content").attr("title", "Keyword to identify the content where you put the link (utm_content)");					
			$("label[for=term]").text("Keyword to identify placement:");									
			$("#utm_term").attr("title", "Keyword to identify placement: for ex. cover, page3. (utm_term)");
			
		} else if ( medium == "cpc-txt" || medium == "cpc-banner" ) {
			$("label[for=content]").text("Keyword to identify the ad.");
			$("#utm_content").attr("title", "Keyword to identify the ad. (utm_content)");							
			$("label[for=term]").text("Word / segment");									
			$("#utm_term").attr("title", "Word of group of words. (utm_term)");
			
		} else {
			$("label[for=content]").text("Content:");
			$("#utm_content").attr("title", "Keyword to identify the content where you will put the link . (utm_content)");			
			$("label[for=term]").text("Word:");				
			$("#utm_term").attr("title", "Keyword to identify the place where you put the link (utm_term)");	
		}
		
	});
	
	$("#campaign").blur(function(){
		var isValidCampaignValue;
		var campaignValue = $("#campaign").val();
		isValidCampaignValue = /^[A-Z][a-z]{1,14}$/.test( campaignValue);
		
		if ( campaignValue!=="" && isValidCampaignValue === false) {
			alert("One lowercased word, starting with an uppercase letter. Max 15 char.");
		}
		
		
	});
	
});

/* ------------ */

$(document).ready(function(){

	$("#login").validate({
		messages: {
			"auth[username]":  {
				required: "The username is required"
			},
			"auth[password]":  {
				required: "The passowrd is required"
			}
		}
	});

});

/* ------------ */

jQuery.ajax({
	
	"url" : "ajax/latest-urls.php",
	"type" : "GET"
	
}).done( function(data) {
	
	var tablehtml = "";
	$.each(data, function( idx, url ) {
		//console.log(url);
		tablehtml = tablehtml + "<tr>" + "<td>" + url.medium +  "</td>" + "<td>" + url.source +  "</td>" + "<td>" + url.campaign +  "</td>" + "<td>" + url.ucontent +  "</td>" + "<td>" + url.bitly +  "</td>" + "</tr>";
	});
	
	$("#tableheaders").after(tablehtml);
	
}).always( function() {
	
}).error( function() {
	
	$("#display_table").html("<h3>Error accessing the URLs database</h3>");
	
});

/* ------------ */

jQuery.ajax({
	
	"url" : "ajax/latest-note.php",
	"type" : "GET"
	
}).done( function(data) {
	
	$("#note").val(data[0].note);
	
}).always( function() {
	
}).error( function() {
	
	$("#note").val("Error accessing the notes database");
	
});

/* ------------ */

$(document).ready(function() {

    var notes_validator = $("#notes").validate({
        
        submitHandler: function(form) {
            
            jQuery.ajax({
                
                "url" : "ajax/insert-note.php",
                "type" : "POST",
                "data" : {
                    "note" : $("textarea[name=note]").val(),
                }
                
            }).done( function(data) {
				
				alert("Saved");
                
            }).always( function() {
                
                
            }).error( function() {
                
                alert("There's an error, please warn");
                
            });
            
          },
        
        /* Error messages */
        messages: {
            note: {
                required: "Don't delete the note"
            }
        }
        
    });

});
